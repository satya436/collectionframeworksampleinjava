package sai.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class ArraylistvsLinkedlist {
	public static void main(String args[])
	{
		Employee e1 =new Employee(1, "sai");
		Employee e2=new Employee(2,"satya");
		List<Employee> aem=new ArrayList<Employee>();
		List<Employee> lem=new LinkedList<Employee>();
		aem.add(e1);
		aem.add(e2);
		lem.addAll(aem);
		Iterator itr=aem.iterator();
		while(itr.hasNext())
		{
			Employee em=(Employee)itr.next();
			System.out.println(em.getId()+" "+em.getName());
		}
		Iterator itr1 =lem.iterator();
		while(itr1.hasNext())
		{
			Employee em1=(Employee)itr1.next();
			System.out.println(em1.getId()+" "+em1.getName());
			
		}
		for (Employee employee1 : lem) {
			System.out.println(employee1.getId()+" "+employee1.getName());
		}
		for (Employee employee2 : aem) {
			System.out.println(employee2.getId()+" "+employee2.getName());
			
		}
	}

}

package sai.collections;

import java.util.HashMap;
import java.util.Map;


public class HashmapSamples {
	public static void main(String args[])
	{
		HashMap<Integer,Employee> hem=new HashMap<Integer,Employee>();
		Employee e1 = new Employee(1,"sai");
		Employee e2=new Employee(1,"satya");
		Employee e3 = new Employee(1,"sai");
		Employee e4 = new Employee(4,"satyasai");
	
		hem.put(1, e1);
		hem.put(2, e2);
		hem.put(3, e3);
		hem.put(4, e4);
		 for(Map.Entry<Integer,Employee> entry:hem.entrySet()){    
		        int key=entry.getKey();  
		        Employee b=entry.getValue();  
		        System.out.println(key+" Details:");  
		        System.out.println(b.getId()+" "+b.getName());   
		    }    
	}


}

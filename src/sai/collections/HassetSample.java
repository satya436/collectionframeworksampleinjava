package sai.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HassetSample {
	public static void main(String args[])
	{
		HashSet<Employee> hem=new HashSet<Employee>();
		Employee e1 = new Employee(1,"sai");
		Employee e2=new Employee(1,"satya");
		Employee e3 = new Employee(1,"sai");
		Employee e4 = new Employee(4,"satyasai");
	
		hem.add(e1);
		hem.add(e2);
		hem.add(e3);
		hem.add(e4);
		for (Employee employee : hem) {
		System.out.println(employee.getId()+" "+employee.getName());	
		}
		
	}

}

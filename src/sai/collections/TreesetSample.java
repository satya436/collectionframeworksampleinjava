package sai.collections;


import java.util.TreeSet;

public class TreesetSample  {

	public static void main(String args[])
	{
		TreeSet<Employee> hem=new TreeSet<Employee>();
		Employee e1 = new Employee(1,"sai");
		Employee e2=new Employee(1,"satya");
		Employee e3 = new Employee(1,"sai");
		Employee e4 = new Employee(4,"satyasai");
	
		hem.add(e1);
		hem.add(e2);
		hem.add(e3);
		hem.add(e4);
		for (Employee employee : hem) {
		System.out.println(employee.getId()+" "+employee.getName());	
		}
		
	}

	
}

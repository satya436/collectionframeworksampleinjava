package sai.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeMap;

public class HashMapVsTreemapPerformance {

	
	public static void main(String args[])
	{
	HashMap<Integer, String> Hmap = new HashMap<>();
	TreeMap<Integer, String> Tmap = new TreeMap<>();

	// Hashmap add
	long startTime = System.nanoTime();

	for (int i = 0; i < 100000; i++) {
		Hmap.put(1, "sai");
	}
	long endTime = System.nanoTime();
	long duration = endTime - startTime;
	System.out.println("Hashmap add:  " + duration);

	// Treemap add
	startTime = System.nanoTime();

	for (int i = 0; i < 100000; i++) {
		Tmap.put(1, "sai");
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("Treemap add: " + duration);

	// Hashmap get
	startTime = System.nanoTime();

	for (int i = 0; i < 10000; i++) {
		Hmap.get(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("Hashmap get:  " + duration);

	// Treemap get
	startTime = System.nanoTime();

	for (int i = 0; i < 10000; i++) {
		Tmap.get(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("Treemap get: " + duration);


	// Hasmap remove
	startTime = System.nanoTime();

	for (int i = 9999; i >=0; i--) {
		Hmap.remove(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("Hashmap remove:  " + duration);

	// Treemap remove
	startTime = System.nanoTime();

	for (int i = 9999; i >=0; i--) {
		Tmap.remove(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("Treemap remove: " + duration);

}
}

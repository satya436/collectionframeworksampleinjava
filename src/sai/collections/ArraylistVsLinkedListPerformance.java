package sai.collections;

import java.util.ArrayList;
import java.util.LinkedList;


/*
The difference of their performance is obvious. LinkedList is faster in add and remove, but slower in get. Based on the complexity table and testing results, we can figure out when to use ArrayList or LinkedList. 
In brief, LinkedList should be preferred if: 
there are no large number of random access of element
there are a large number of add/remove operations */
public class ArraylistVsLinkedListPerformance {
	
	public static void main(String args[])
	{
	ArrayList arrayList = new ArrayList();
	LinkedList linkedList = new LinkedList();

	// ArrayList add
	long startTime = System.nanoTime();

	for (int i = 0; i < 100000; i++) {
	arrayList.add(i);
	}
	long endTime = System.nanoTime();
	long duration = endTime - startTime;
	System.out.println("ArrayList add:  " + duration);

	// LinkedList add
	startTime = System.nanoTime();

	for (int i = 0; i < 100000; i++) {
	linkedList.add(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("LinkedList add: " + duration);

	// ArrayList get
	startTime = System.nanoTime();

	for (int i = 0; i < 10000; i++) {
	arrayList.get(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("ArrayList get:  " + duration);

	// LinkedList get
	startTime = System.nanoTime();

	for (int i = 0; i < 10000; i++) {
	linkedList.get(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("LinkedList get: " + duration);


	// ArrayList remove
	startTime = System.nanoTime();

	for (int i = 9999; i >=0; i--) {
	arrayList.remove(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("ArrayList remove:  " + duration);

	// LinkedList remove
	startTime = System.nanoTime();

	for (int i = 9999; i >=0; i--) {
	linkedList.remove(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("LinkedList remove: " + duration);

}
}
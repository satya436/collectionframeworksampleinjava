package sai.collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class HashsetVsTreesetperformance {
	
	public static void main(String args[])
	{
	HashSet Hset = new HashSet();
	TreeSet Tset = new TreeSet();

	// Hashset add
	long startTime = System.nanoTime();

	for (int i = 0; i < 100000; i++) {
		Hset.add(i);
	}
	long endTime = System.nanoTime();
	long duration = endTime - startTime;
	System.out.println("Hashset add:  " + duration);

	// Treeset add
	startTime = System.nanoTime();

	for (int i = 0; i < 100000; i++) {
		Tset.add(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("Treeset add: " + duration);

	// Hashset get
	startTime = System.nanoTime();

	for (int i = 0; i < 10000; i++) {
		Hset.getClass();
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("Hashset get:  " + duration);

	// Treeset get
	startTime = System.nanoTime();

	for (int i = 0; i < 10000; i++) {
		Tset.getClass();
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("Treeset get: " + duration);


	// Hashset remove
	startTime = System.nanoTime();

	for (int i = 9999; i >=0; i--) {
		Hset.remove(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("Hashset remove:  " + duration);

	// Treeset remove
	startTime = System.nanoTime();

	for (int i = 9999; i >=0; i--) {
		Tset.remove(i);
	}
	endTime = System.nanoTime();
	duration = endTime - startTime;
	System.out.println("Treeset remove: " + duration);

}

}
